
# Average Reward Reinforcement Learning

This repository offers a comprehensive library of research code developed for reinforcement learning (RL) studies, with a focus on **average reward** scenarios.  
The codebase is fully compatible with the **Gymnasium API**, facilitating the evaluation of novel algorithms across diverse environments and enabling the generation of experiments and regret performance plots.

## Features

- **Algorithm Testing**: Implement and assess new RL algorithms in various settings.
- **Experimentation**: Design and execute experiments to evaluate algorithm performance.
- **Performance Visualization**: Generate regret performance plots to visualize and compare algorithm efficacy.

## Repository Structure

- **`main/article_companions/`**: Contains companion code to reproduce experiments from corresponding research articles.
- **`main/templates/`**: Provides template scripts:
  - A **bandit experiment template** for testing algorithms in multi-armed bandit settings.
  - A **Markov Decision Process (MDP) experiment template** for evaluating RL algorithms in MDP environments.

## Getting Started

1. **Clone the Repository**:

   ```bash
   git clone https://gitlab.inria.fr/omaillar/average-reward-reinforcement-learning.git
   cd average-reward-reinforcement-learning
   ```

2. **Install Dependencies**:

   Ensure you have Python installed. It's recommended to use a virtual environment:

   ```bash
   python3 -m venv venv
   source venv/bin/activate
   pip install -r requirements.txt
   ```

3. **Run Experiments**:

   - Browse `main/templates/` to use pre-configured experiment templates for bandits and MDPs.
   - Navigate to `main/article_companions/` for companion code to reproduce experiments from specific research papers.

## Citation

If you utilize this codebase in your research, please consider citing the relevant articles associated with the companion code.  
Citation details can be found within the respective directories in `main/article_companions/`.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgments

We extend our gratitude to the contributors and the open-source community for their invaluable support and resources.

---

This version maintains a structured, academic tone while improving clarity and engagement for researchers. Let me know if you’d like any refinements! 🚀