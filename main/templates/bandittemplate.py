import gymnasium
import learners.MABs.utils
from experiments.fullExperiment import runLargeMulticoreExperiment

#######################
# Import registered environments
import environments.MABs.registerEnvironments as bW
# To get the list of registered environments:
print("List of registered environments: ")
[print(k) for k in bW.registerWorlds.keys()]

# or Import directly some environment
import environments.MDPs_discrete.envs.RandomMDP as drmdp

#######################
# Import some learners
#import learners.Generic.Random as random
import learners.MABs.IMED as imed
import learners.MABs.UCB as ucb
import learners.MABs.Uniform as uniform
import learners.MABs.Oracle as Or


#######################
# Instantiate one environment
#env = bW.makeWorld(bW.registerWorld('grid-2-room'))
#env = bW.makeWorld(bW.registerWorld('grid-4-room'))
#env = bW.makeWorld(bW.registerWorld('river-swim-6'))
#env = bW.makeWorld(bW.registerWorld('grid-random'))
# TODO: Register is as gymnasium, etc.
env = gymnasium.make(bW.register_MABs('mab-bernoulli'))


#######################
# Instantiate a few learners to be compared:
agents = []
nS = env.observation_space.n
nA = env.action_space.n
delta=0.05
agents.append( [imed.IMED, {"nbArms":nA,"kullback": learners.MABs.utils.klBern}])
agents.append( [ucb.UCB, {"nbArms":nA, "delta":lambda t: 0.01/(t*(t+1)) }])
agents.append( [uniform.UE, {"nA":nA}])


#############################
# Compute oracle policy:
oracle = Or.Oracle(env)

#######################
# Run a full experiment
#######################
import os
from experiments.utils import get_project_root_dir
ROOT= get_project_root_dir()+"/main/templates/"+"bandittemplate"+"_results/"
os.mkdir(ROOT)

runLargeMulticoreExperiment(env, agents, oracle, timeHorizon=5000, nbReplicates=16,root_folder=ROOT)

#######################
# Plotting Regret directly from dump files of past runs:
#files = plR.search_dump_cumRegretfiles("RiverSwim-6-v0", root_folder=ROOT)
#plR.plot_results_from_dump(files, 500)
